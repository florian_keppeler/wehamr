[![pipeline status](https://gitlab.com/fvafrcu/wehamr/badges/master/pipeline.svg)](https://gitlab.com/fvafrcu/wehamr/commits/master)    
[![coverage report](https://gitlab.com/fvafrcu/wehamr/badges/master/coverage.svg)](https://gitlab.com/fvafrcu/wehamr/commits/master)
<!-- 
    [![Build Status](https://travis-ci.org/fvafrcu/wehamr.svg?branch=master)](https://travis-ci.org/fvafrcu/wehamr)
    [![Coverage Status](https://codecov.io/github/fvafrcu/wehamr/coverage.svg?branch=master)](https://codecov.io/github/fvafrcu/wehamr?branch=master)
-->
[![CRAN_Status_Badge](https://www.r-pkg.org/badges/version/wehamr)](https://cran.r-project.org/package=wehamr)
[![RStudio_downloads_monthly](https://cranlogs.r-pkg.org/badges/wehamr)](https://cran.r-project.org/package=wehamr)
[![RStudio_downloads_total](https://cranlogs.r-pkg.org/badges/grand-total/wehamr)](https://cran.r-project.org/package=wehamr)

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, echo = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "README-"
)
```

# wehamr
## Introduction
Please read the
[vignette](https://fvafrcu.gitlab.io/wehamr/doc/An_Introduction_to_wehamr.html).
<!-- 
[vignette](https://CRAN.R-project.org/package=wehamr/vignettes/An_Introduction_to_wehamr.html).

-->

Or, after installation, the help page:
```{r, eval = FALSE}
help("wehamr-package", package = "wehamr")
```
```{r, echo = FALSE}
  # insert developement page
  help_file <- file.path("man", "wehamr-package.Rd")
  captured <- gsub('_\b', '',  capture.output(tools:::Rd2txt(help_file) ))
  cat(captured, sep = "\n")
```

## Installation

You can install wehamr from gitlab via:

```{r gh-installation, eval = FALSE}
if (! require("remotes")) install.packages("remotes")
remotes::install_gitlab("fvafrcu/wehamr")
```


