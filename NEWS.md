# wehamr 0.4.0.9000

* FIXME

# wehamr 0.4.0
    Cleaned WPKSKW remainders
    
    R/internal.R contains the single survivor from R/misc.R, list2data.frame().
    
    renamed:    R/steuer.R -> R/control.R
    deleted:    R/ggplot2.R
    new file:   R/internal.R
    deleted:    R/misc.R
    deleted:    R/multiplot.R
    deleted:    R/test_data.R
    deleted:    R/volume.R
    deleted:    R/wrap_weham.R
# wehamr 0.3.1
- Added function `query_weham_version` which queries the version of the WEHAM exe provided.
- Added function `provide_weham_templates`. It returns the paths to templates for control, 
   output and output-sql databases fitting for weham versions 2 and 3 accordingly.

# wehamr 0.3.0
- Now writing weham.ini to output dircetory.
- Now coming with all files needed to run weham (3.0.1).
- Changed former arguments to `run_weham` from `run_dir` to `output_directory` and
  from `weham_root` to `run_dir`, which is hopefully more sane. 

# wehamr 0.2.0
- Default standard run works.

# wehamr 0.1.0

* Added a `NEWS.md` file to track changes to the package.



