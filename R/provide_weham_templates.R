provide_weham_templates <- function(exe, type = c("control", "output", "output_sql")) {
  type <- match.arg(type)
  version <- query_weham_version(exe)
  if (utils::compareVersion(version, "3.0.1") >= 0) {
    directory <- system.file("weham", package = "wehamr")
  } else {
    directory <- system.file("weham", "weham2", package = "wehamr")
  }
  path <- switch(type,
                 "control" = list.files(directory, pattern = "^weham_steuer..\\.mdb$", full.names = TRUE),
                 "output" = file.path(directory, "weham_outputtmpl.mdb"),
                 "output_sql" = file.path(directory, "weham_outputsqltmpl.mdb"),
                 )
  return(path)
  
}
