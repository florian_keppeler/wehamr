#' Query the version of WEHAM
#' 
#' WEHAM versions prior to version 2.0.5.1 and versions 3 but prior to 3.0.1 did not report 
#' their version. If reporting fails, we assume it to be 2.0.5.1, expecting no (developement) 
#' versions > 3 but < 3.0.1 to be around (there never has been 3.0.0, only versios like 3.0.0.7).
#' @param exe The executable of WEHAM to be queried
#' @return  The version reported by WEHAM, or, if none, "2.0.5.1"
#' @export
#' @examples 
#' query_weham_version(exe = system.file("weham", "weham2", "weham2012.exe", package = "wehamr"))
#' query_weham_version(system.file("weham", "weham_Klima.exe", package = "wehamr"))
query_weham_version <- function(exe = system.file("weham", "weham_Klima.exe", package = "wehamr")) {
  # we need a weham.ini where the exe file lives:
  temp_dir <- tempfile()
  dir.create(temp_dir)
  on.exit(unlink(temp_dir, recursive = TRUE))
  wd <- setwd(temp_dir)
  on.exit(setwd(wd))
  file.copy(system.file("weham", "weham.ini", package = "wehamr"), "weham.ini")
  file.copy(list.files(system.file("weham", "dll", package = "wehamr"), full.names = TRUE), ".",
            overwrite = TRUE)
  # now we have one.
  weham_version <- tryCatch(system2(exe, args = "--version", stdout = TRUE, timeout = 1),
                            warning = function(w) return("2.0.5.1"))
  weham_version <- sub("^VERSION ?", "", weham_version)
  return(weham_version)
}
